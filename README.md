# Draw IO Exporter

Created from: <https://gitlab.univ-nantes.fr/bousse-e/docker-drawio> and <https://github.com/rglaue/plantuml_to_drawio>

## Build and push

```
docker build . -t woranhun/drawio-gitlab-ci
docker push woranhun/drawio-gitlab-ci
```
