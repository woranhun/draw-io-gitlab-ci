FROM fedora:35

RUN dnf install -y xorg-x11-server-Xvfb alsa-lib make findutils gdouros-symbola-fonts google-noto-emoji-fonts google-noto-emoji-color-fonts unzip rsync \
    && dnf group install -y fonts \
    && dnf install -y https://github.com/jgraph/drawio-desktop/releases/download/v20.3.0/drawio-x86_64-20.3.0.rpm \
    #for plantuml
    && dnf install -y java-17-openjdk-headless.x86_64 perl wget \ 
    && dnf clean all \
    && rm -rf /var/cache/dnf
COPY ./drawio /usr/local/bin/drawio

# RUN wget https://github.com/marp-team/marp-cli/releases/download/v2.2.0/marp-cli-v2.2.0-linux.tar.gz -O marp.tar.gz \
#    && tar -xvzf marp.tar.gz \
#    && cp marp /usr/local/bin/marp


#for plantuml
COPY ./convert_plantuml_to_drawio.pl /convert_plantuml_to_drawio.pl
RUN wget https://github.com/plantuml/plantuml/releases/download/v1.2022.5/plantuml-1.2022.5.jar\
    && ln -s /convert_plantuml_to_drawio.pl /usr/local/bin/convert_plantuml_to_drawio.pl
